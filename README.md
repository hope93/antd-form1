在用antd的时候，我们如果要对表单进行添加和删除该怎么弄呢，如下：

```import React from 'react';
import { connect } from 'dva';
import { Form, Input, Button } from 'antd';
import styles from './eg1.css';

const FormItem = Form.Item;

function Page(props) {
  const { form } = props;
  const { getFieldDecorator, getFieldValue } = form

  // 表单提交
  const handleSubmit = (e) => {
    e.preventDefault();
    form.validateFields((err, values) => {
      if (!err) {
        console.log(values);
      }
    });

  }

  // 添加
  const add = () => {
    const list = form.getFieldValue('list');
    const nextList = list.concat({});
    form.setFieldsValue({
      list: nextList,
    });


  }

  // 删除
  const deleteRow = (index) => {
    const list = form.getFieldValue('list');
    const content = form.getFieldValue('content');

    if (list.length === 1) {
      return;
    }

    form.setFieldsValue({
      list: list.filter((item, key) => key !== index),
      content: content.filter((item, key) => key !== index),
    });



  }

  getFieldDecorator('list', { initialValue: [{}] });
  const list = getFieldValue('list');

  const listContent = list.map((item, index) => {
    return (
      <FormItem label='名称：' key={index}>
      {getFieldDecorator(`content[${index}].name`, {
         rules: [{
         required: true,
         message: "名称不能为空！",
         }],
      })(
         <Input placeholder="请输入名称" style={{ width: '60%', marginRight: 8 }} />
      )}

       {index > 0 ? (
           <Button type="primary" onClick={deleteRow.bind(this, index)}>删除</Button>
       ) : null}


      </FormItem>
    );
  });


  return (
    <div className={styles.normal}>
        <Form onSubmit={handleSubmit}>

        {listContent}
        <FormItem>
          <Button type="primary" htmlType="submit">提交</Button>
          <Button type="primary" style={{ marginLeft: '10px' }} onClick={add}>增加</Button>
        </FormItem>
      </Form>

    </div>
  );
}


const page = Form.create()(Page);
export default connect()(page);

```

这里不仅能对表单进行增加和删除，还能对表单进行验证，看是否有输入，以上是本身没有数据的情况，如果是有数据的情况如下：

```
import React from 'react';
import { connect } from 'dva';
import { Form, Input, Button } from 'antd';
import styles from './eg2.css';

const FormItem = Form.Item;

function Page(props) {
  const { form } = props;
  const { getFieldDecorator, getFieldValue } = form

  // 表单提交
  const handleSubmit = (e) => {
    e.preventDefault();
    form.validateFields((err, values) => {
      if (!err) {
        console.log(values);
      }
    });

  }

  // 添加
  const add = () => {
    const list = form.getFieldValue('list');
    const nextList = list.concat({});
    form.setFieldsValue({
      list: nextList,
    });


  }

  // 删除
  const deleteRow = (index) => {
    const list = form.getFieldValue('list');
    const content = form.getFieldValue('content');

    if (list.length === 1) {
      return;
    }

    form.setFieldsValue({
      list: list.filter((item, key) => key !== index),
      content: content.filter((item, key) => key !== index),
    });



  }


  const slist = [{
    id:'0001',
    name: '黎明'
  }, {
    id:'0002',
    name: '晴天'
  }]
  getFieldDecorator('list', { initialValue: slist });
  const list = getFieldValue('list');

  const listContent = list.map((item, index) => {
    getFieldDecorator(`content[${index}].id`, {initialValue: item.id || ''})
    return (
      <FormItem label='名称：' key={index}>
      {getFieldDecorator(`content[${index}].name`, {
         rules: [{
         required: true,
         message: "名称不能为空！",
         }],
         initialValue: item.name || ''
      })(
         <Input placeholder="请输入名称" style={{ width: '60%', marginRight: 8 }} />
      )}

       {index > 0 ? (
           <Button type="primary" onClick={deleteRow.bind(this, index)}>删除</Button>
       ) : null}


      </FormItem>
    );
  });


  return (
    <div className={styles.normal}>
        <Form onSubmit={handleSubmit}>

        {listContent}
        <FormItem>
          <Button type="primary" htmlType="submit">提交</Button>
          <Button type="primary" style={{ marginLeft: '10px' }} onClick={add}>增加</Button>
        </FormItem>
      </Form>

    </div>
  );
}


const page = Form.create()(Page);
export default connect()(page);

```

一般如果本身有数据，都会有每行数据的id，但是这个id不显示，我们都会用getFieldDecorator给id声明，这样在我们提交表单的时候，就可以得到表单抓取到id的数据，有数据跟没有数据的差别就是，有数据需要在表单getFieldDecorator的时候给一个初始值，其他两者都一样

具体代码下载地址：https://gitee.com/hope93/antd-form1
